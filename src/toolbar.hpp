#include <Elementary.h>
#include <vector>

namespace Kensho
{
    class Toolbar
    {
        public:

            Toolbar();
            ~Toolbar();
            void append_item(void);

        private:

            //The current appended items
            std::vector<Evas_Object*> items_;
    };
}
