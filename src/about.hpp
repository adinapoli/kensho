#include <Elementary.h>
#include <string>

const std::string about = "Kensho Editor 0.1 - Created by Alfredo Di Napoli.";

extern "C" {
/**
 * Show (and make visible) the about msg
 */
void show_about(void *data, Evas_Object *obj, void *event_info);
}
