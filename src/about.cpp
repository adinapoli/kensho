#include "about.hpp"

void show_about(void *data, Evas_Object *obj, void *event_info)
{
    Evas_Object *bubble = (Evas_Object*)data;
    Evas_Object *msg_label = elm_object_content_get(bubble);
    elm_object_text_set(msg_label, about.c_str());
    evas_object_show(bubble);
}
