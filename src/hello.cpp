#include <Elementary.h>
#include <string>
#include "about.hpp"

static void win_del(void *data, Evas_Object *obj, void *event_info)
{
   elm_exit();
}

EAPI int elm_main(int argc, char **argv)
{
   Evas_Object *win, *bg, *lb, *tb;

   win = elm_win_add(NULL, "hello", ELM_WIN_BASIC);
   elm_win_title_set(win, "Kensho");
   evas_object_smart_callback_add(win, "delete,request", win_del, NULL);

   //Set a default background
   bg = elm_bg_add(win);
   evas_object_size_hint_weight_set(bg, 1.0, 1.0);
   elm_win_resize_object_add(win, bg);
   evas_object_show(bg);

   //Add a toolbar

   //Add a label
   lb = elm_label_add(win);
   std::string lb_caption = "Hello World!";
   elm_object_text_set(lb, lb_caption.c_str());
   evas_object_show(lb);

   //Add a bubble
   Evas_Object *bubble;

   //Bubble logic
   bubble = elm_bubble_add(win);
   elm_object_content_set(bubble, lb);
   evas_object_resize(bubble, 300, 100);

   //Add a simple button
   Evas_Object *btn;
   btn = elm_button_add(win);
   elm_object_text_set(btn, "Click me");
   evas_object_move(btn, 50, 150);
   evas_object_resize(btn, 50, 25);
   elm_win_autodel_set(win, EINA_TRUE);
   evas_object_show(btn);
   evas_object_smart_callback_add(btn, "clicked", show_about, bubble);


   //Show the parent
   evas_object_resize(win, 640, 480);
   evas_object_show(win);

   elm_run();
   elm_shutdown();
   return 0;
}
ELM_MAIN()
